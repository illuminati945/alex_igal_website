<?php
session_start();
if (!isset($_SESSION['currentAdmin'])) {

    header('HTTP/1.0 403 Forbidden');
    die('You are not allowed to access this file.');     
}

include("php/myphpadmin/fetch.php");

?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="../StyleSheet.css" rel="stylesheet" />
</head>

<body>
    <div class="topnav">
        <a href="index.php">בית-תפריט</a>
        <a href="hazmanot1.html">הזמנות</a>
        <a href="manager.php">תצוגת מנהל</a>
        <a href='status.html'>סטאטוס הזמנה</a>
        <a href='payment.html'>דף תשלום</a>

        
        <a href="php/logout.php" class="admin-only">logout [<?php echo $_SESSION['currentAdmin']?>]</button>
        <a href='php/table_orders.php' class="admin-only active">שולחנות</a>

    </div>

    <div class="container">
        <div class="row" style="padding-top: 10%;">
            <div class="col-sm-8">
                <?php echo $deleteMsg ?? ''; ?>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>

                                <th>name</th>
                                <th>people_count</th>
                                <th>date</th>
                                <th>area</th>
                        </thead>
                        <tbody>
                            <?php
                            if (is_array($fetchData)) {
                                $sn = 1;
                                foreach ($fetchData as $data) {
                            ?>
                                    <tr>
                                        <td><?php echo $data['name'] ?? ''; ?></td>
                                        <td><?php echo $data['people_count'] ?? ''; ?></td>
                                        <td><?php echo $data['date'] ?? ''; ?></td>
                                        <td><?php echo $data['area'] ?? ''; ?></td>
                                    </tr>
                                <?php
                                    $sn++;
                                }
                            } else { ?>
                                <tr>
                                    <td colspan="8">
                                        <?php echo $fetchData; ?>
                                    </td>
                                <tr>
                                <?php
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>