<?php

session_start();
$_SESSION["test"] = "Test";

extract($_POST);

define("MAX_USERNAME_SIZE",     10);
define("MIN_USERNAME_SIZE",      0);



$admins = [
    "admin" => "password",
    "manager" => "1234"
];


if(isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header('location:login.php');
}

if (isset($username)) {

    $username = validate($username);
    $password = validate($password);

    $is_admin = is_admin($username, $password);
    $result = $is_admin;

    echo $result;
} else {
    echo "ERROR: username unset";
}

function is_admin($username, $password)
{
    global $admins;

    if (strlen($username) - MIN_USERNAME_SIZE > -1 && MAX_USERNAME_SIZE - strlen($username) > -1) {
        foreach ($admins as $key => $val) {
            if ($key == $username && $val == $password) {
                // $current_admin = $username;
                $_SESSION["currentAdmin"] = $username;

                return true;
            }
        }
    }

    return false;
}

function validate($value)
{
    $value = trim($value);
    $value = stripslashes($value);
    $value = htmlspecialchars($value);

    return $value;
}
