﻿<?php
session_start();

function get_username()
{
    echo isset($_SESSION['currentAdmin']) ? $_SESSION['currentAdmin'] : "";
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />

    <title>FINAL HW</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <style>
        label {
            display: block;
            font: .9rem 'Fira Sans', sans-serif;
        }

        input[type='submit'],
        label {
            margin-top: 1rem;
        }

        .container {
            font-family: Calibri;
            text-align: center;
        }

        #header {
            background-color: #00d6ff;
            font-size: 25px;
            font: bold;
        }

        .card-img-top {
            height: 150px;
        }

        .img {
            height: 50px;
            width: 50px;
        }

        #center-div {
            height: 250px;
            width: 250px;
        }
    </style>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="JavaScript.js"></script>
    <link href="StyleSheet.css" rel="stylesheet" />
</head>

<body>


    <div class="topnav">
        <a href="index.php">בית-תפריט</a>
        <a href="hazmanot1.html">הזמנות</a>
        <a class="active" href="manager.php">תצוגת מנהל</a>
        <a href='status.html'>סטאטוס הזמנה</a>
        <a href='payment.html'>דף תשלום</a>

        
        <a href="php/auth/logout.php" class="admin-only">logout [<?php get_username();?>]</button>
        <a href='table_orders.php' class="admin-only">שולחנות</a>

    </div>

    <div class="alert alert-primary alert-dismissible fade show" role="alert" id="welcome_alert" style="display:none">
        test
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>


    <script>
        $(document).ready(function() {
            $("#admin_login_form").submit(init_manager);
            admin_logged_in();
        });
    </script>


    <div>
        <div class="container">


            <div class="row">
                <div id="check" class="d-flex pt-4">

                    <!--<input id="check0" type="checkbox" name="check0" value="check0">
                    <label for="check0">מנהל בודק את ההזמנה</label><br>
                    <input id="check1" type="checkbox" name="check1" value="check1">
                    <label for="check1"> הזמנה התקבלה</label><br>
                    <input id="check2" type="checkbox" name="check2" value="check2">
                    <label for="check2"> הזמנה בהכנה</label><br><br>
                    <input id="check3" type="checkbox" name="check3" value="check3">
                    <label for="check3"> הזמנה מוכנה</label><br><br>
                    <input id="check4" type="checkbox" name="check4" value="check4">
                    <label for="check4"> משלוח יצא לדרך</label>-->
                    <!--<br><br>
                    <input type="button" value="Submit" onclick="check()">-->

                </div>
                <div id="left-div" class="col">





                    <form id="admin_login_form" action="post">
                        <div>
                            <div>
                                <label for="username">Username:</label>
                                <input type="text" id="username" name="username">
                            </div>

                            <div>
                                <label for="pass">Password (4 characters minimum):</label>
                                <input type="password" id="password" name="password" minlength="4">
                            </div>

                            <input type="submit" name="admin_login" value="Sign in">
                        </div>



                    </form>
                </div>

                <div id="center-div" class="col-12">


                    <div class="row">
                        <div id="new" class="col">
                            <h1 style="font-weight:900;text-decoration:underline;text-align:center;color:red">
                                מנות
                                עיקריות
                            </h1>
                        </div>

                    </div>


                    <button type="button" onclick="add_main()" class="btn btn-danger admin-only">הוסף עיקרית</button>

                    <div id="main_cource">

                        <!-- עיקריות -->

                    </div>

                    <div>

                        <h1 style="font-weight:900;text-decoration:underline;text-align:center;color:red">קינוחים</h1>
                    </div>
                    <button type="button" onclick="add_dessert()" class="btn btn-warning admin-only">הוסף קינוח</button>

                    <div id="dessert">

                        <!-- קינוחים -->

                    </div>


                </div>




                <div id="right_div" class="col-xs-12 d-md-none d-lg-block col-xl-2"></div>




            </div>

        </div>
    </div>



    <script>
        function refresh() {

            if (localStorage["arabic_resturant_2"] != undefined) {
                arabic_resturant_2 = JSON.parse(localStorage["arabic_resturant_2"]);
                arabic_resturant = arabic_resturant_2
            }
            var str = ""

            for (var i = 0; i < arabic_resturant.dishes.main_cource.length; i++) {

                str += `<div class="d-flex">`
                for (var ii = 0; ii < 2; ii++) {

                    if (ii == 1) {
                        // if (arabic_resturant.dishes.main_cource.length == (i - 1) && i % 2 == 0
                        //     || ii < arabic_resturant.dishes.main_cource.length - 1)

                        if (arabic_resturant.dishes.main_cource[i + 1])
                            i = i + 1
                        else
                            break
                    }
                    str += `
                                        <div id=main_cource${i} class="col" style="
                overflow-wrap: anywhere;
" >
                                            <img src="New folder/${arabic_resturant.dishes.main_cource[i].picture}"class="col img-responsive" style="width: 200px;
                    height: 150px;" />

<h1>${arabic_resturant.dishes.main_cource[i].name}</h1>

                    <h5> ${arabic_resturant.dishes.main_cource[i].price}$</h5>

                        <h4>${arabic_resturant.dishes.main_cource[i].description}   </h4>

                                <h5>${arabic_resturant.dishes.main_cource[i].type}</h5>


                                   <p>
                                   <button onclick='clear_item("${arabic_resturant.dishes.main_cource[i].name}" ,"main_cource", "main_cource${i}")'>מחק מנה</button>
                                   </p >
                         </div>
`
                }
                str += "</div>"



            }
            document.getElementById("main_cource").innerHTML = str;

            str = ""
            for (var i = 0; i < arabic_resturant.dishes.dessert.length; i++) {

                str += `<div class="d-flex">`
                for (var ii = 0; ii < 2; ii++) {

                    if (ii == 1) {
                        if (arabic_resturant.dishes.dessert[i + 1])
                            i = i + 1
                        else {
                            break
                        }
                    }

                    str += `
                                        <div id=dessert${i} class="col" style="
                overflow-wrap: anywhere;
" >
                                            <img src="New folder/${arabic_resturant.dishes.dessert[i].picture}"class="col img-responsive" style="width: 200px;
                    height: 150px;" />

<h1>${arabic_resturant.dishes.dessert[i].name}</h1>

                    <h5> ${arabic_resturant.dishes.dessert[i].price}$</h5>

                        <h4>${arabic_resturant.dishes.dessert[i].description}   </h4>

                                <h5>${arabic_resturant.dishes.dessert[i].type}</h5>


                                   <p>
                                   <button onclick='clear_item("${arabic_resturant.dishes.dessert[i].name}", "dessert","dessert${i}")'>מחק מנה</button>
                                   </p >
                         </div>
`
                }
                str += "</div>"



            }
            document.getElementById("dessert").innerHTML = str;


        }

        var request;

        function init_manager(event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            var $form = $(this);

            var serializedData = $form.serialize();

            request = $.ajax({
                url: 'php/auth/admin.php',
                type: "post",
                data: serializedData
            });

            request.done(function(result, textStatus, jqXHR) {
                if (result == 1) {
                    location.reload();
                } else if (result == 0) {
                    alert("שם משתמש או סיסמא אינם נכונים אנא נסה שוב");
                } else {
                    console.log(result);
                    alert("ERROR. check console.")
                }
            });

            // Callback handler that will be called on failure
            request.fail(function(jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.error(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
            });
        }

        function clear_item(name, y, item) {

            if (localStorage["arabic_resturant_2"] != undefined) {
                arabic_resturant_2 = JSON.parse(localStorage["arabic_resturant_2"]);
                arabic_resturant_1 = arabic_resturant_2
            }

            var x = arabic_resturant_1 /*JSON.parse(localStorage.getItem("arabic_resturant_1"))*/
            for (var i = 0; i < x.dishes[y].length; i++) {
                var mana = x.dishes[y][i]
                if (x.dishes[y][i].name == name) {


                    x.dishes[y].splice(i, 1);
                    document.getElementById(item).remove()

                    localStorage.setItem("arabic_resturant_1", JSON.stringify(x))
                    localStorage.setItem("arabic_resturant_2", JSON.stringify(x))
                    alert
                    break;
                }
            }

        }



        function check() {
            var check_test = [];
            for (i = 0; i < 5; i++) {
                if (document.getElementById("check" + i).checked == true)
                    check_test[i] = 'class="progtrckr-done"';
                else if (document.getElementById("check" + i).checked == false) {
                    check_test[i] = 'class="progtrckr-todo"';


                }
            }

            localStorage.setItem("check_test", JSON.stringify(check_test));


        }

        function admin_logged_in() {
            var username = "<?php echo isset($_SESSION['currentAdmin']) ? $_SESSION['currentAdmin'] : ""; ?>";
            if (username != "") {
                // alert("welcome " + username);

                $(".admin-only").each(function(index) {
                    $(this).show();
                });

                $("#admin_login_form").hide();
                
                var alert = $("#welcome_alert");
                var curr_text = alert.html();

                console.log(curr_text);
                curr_text = curr_text.replace("test", "Welcome, " + username);
                console.log(curr_text);

                alert.html(curr_text);
                // $('#welcome_alert').html("<div class='alert alert-error'>Welcome, " + username +"</div>");
                $('#welcome_alert').show();
                // $("#welcome_alert").alert();

                refresh();
            } else {
                $(".admin-only").each(function(index) {
                    $(this).hide();
                });
            }
        }
    </script>



    <script src="./add.js"></script>

    <script>
        /*function ceckbox()*/
        {
            if (localStorage["check_test"] != undefined) {
                var stored_check_test = JSON.parse(localStorage.getItem("check_test"));
                var valus = ["מנהל בודק את ההזמנה", "הזמנה התקבלה",
                    "הזמנה בהכנה",
                    "הזמנה מוכנה",
                    "משלוח יצא לדרך"
                ]
                var str = ""
                for (var i = 0; i < stored_check_test.length; i++) {
                    if (stored_check_test[i] == 'class=\"progtrckr-done\"') {
                        str += `<input id="check${i}" type="checkbox" name="check${i}" checked value="check${i}">
                                <label for="check${i}">${valus[i]}</label><br>`
                    } else {
                        str += `<input id="check${i}" type="checkbox" name="check${i}"  value="check${i}">
                                <label for="check${i}">${valus[i]}</label><br>`
                    }

                }
                str += `
<br><br>
                                <input type="button" value="Submit" onclick="check()">
`

                document.getElementById("check").innerHTML = str;
            } else {
                str = "";
                str += `
                            <input id="check0" type="checkbox" name="check0" value="check0">
                                <label for="check0">מנהל בודק את ההזמנה</label><br>
                                    <input id="check1" type="checkbox" name="check1" value="check1">
                                        <label for="check1"> הזמנה התקבלה</label><br>
                                            <input id="check2" type="checkbox" name="check2" value="check2">
                                                <label for="check2"> הזמנה בהכנה</label><br><br>
                                                    <input id="check3" type="checkbox" name="check3" value="check3">
                                                        <label for="check3"> הזמנה מוכנה</label><br><br>
                                                            <input id="check4" type="checkbox" name="check4" value="check4">
                                                                <label for="check4"> משלוח יצא לדרך</label> `
                str += `
<br><br>
                                <input type="button" value="Submit" onclick="check()">`
                document.getElementById("check").innerHTML = str;

            }
        }
    </script>
</body>

</html>