﻿
special_set_of_labels = [];
special_set_of_labels[0] = new Object();
special_set_of_labels[0].name = "בשרי";

special_set_of_labels[1] = new Object();
special_set_of_labels[1].name = "צמחוני";

special_set_of_labels[2] = new Object();
special_set_of_labels[2].name = "חריף";

special_set_of_labels[3] = new Object();
special_set_of_labels[3].name = "חלבי"
special_set_of_labels[4] = new Object();
special_set_of_labels[4].name = "ספיישל השף ";



arabic_resturant = {
    dishes: {
        main_cource: [
            { name: "מלפוף", description: "כרוב ממול אורז", price: 45, picture: 'MALFOF.jpeg', type: special_set_of_labels[1].name },
            { name: "שישברק", description: "כיסוני בצק ממולאי בשר", price: 55, picture: 'SHISHBARAK.jpeg', type: special_set_of_labels[4].name },
            { name: "פטאייר", description: "מאפה ממולא תרד", price: 43, picture: 'PATAER.jpeg', type: special_set_of_labels[1].name },
            { name: "חומוס", description: "חומוס פטריות", price: 47, picture: 'HOMOS.jpeg', type: special_set_of_labels[2].name },
            { name: "מסאחן", description: "עוף על מצע פיתה", price: 62, picture: 'MASAHAN.jpeg', type: special_set_of_labels[0].name },
            { name: "עראיס", description: "פיתה במילוי בשר טחון", price: 60, picture: 'ARAES.jpeg ', type: special_set_of_labels[0].name },
            { name: "קובה", description: "בשר מטוגן במעטפת בורגול", price: 48, picture: 'KOBA.jpeg', type: special_set_of_labels[0].name },
            { name: "מקלובה", description: "תבשיל אורז וירקות", price: 50, picture: 'MAKLOBA.jpeg', type: special_set_of_labels[1].name },
            { name: "לחמעגון", description: "מאפה בשר פתוח", price: 50, picture: 'LAHMAJON.jpeg', type: special_set_of_labels[0].name },
            { name: "עליגפן", description: "עלי גפן במילוי אורז", price: 40, picture: 'ALAI_GEFEN.jpeg', type: special_set_of_labels[1].name },
        ],
        dessert: [
            { name: "כנאפה", description: "קינוח שערות קדאיף וגבינה", price: 45, picture: 'KNAFA.jpeg', type: special_set_of_labels[3].name },
            { name: "בסבוסה", description: "עוגת סולת דבש ושקדים", price: 40, picture: 'OGAT_SOLAT_DVASH.jpeg', type: special_set_of_labels[4].name },
            { name: "לילותביירות", description: "עוגת סולת ושמנת", price: 35, picture: 'OGAT_SOLAT__HEAVY_CREAM.jpeg', type: special_set_of_labels[3].name },
            { name: "קטאייף", description: "פנקייק ממולא גבינה", price: 38, picture: 'KATAEF.jpeg', type: special_set_of_labels[3].name },
            { name: "בקלוואה", description: "מאפה-בצק דק ואגוזים", price: 40, picture: 'BAKLAWA.jpeg', type: special_set_of_labels[1].name },
            { name: "רחתלוקום", description: "ממתק בטעמי פירות", price: 37, picture: 'RAHAT_LOKOM.jpeg', type: special_set_of_labels[1].name },
            { name: "עואמה", description: "סופגניות ביס", price: 36, picture: 'AWAMA.jpeg', type: special_set_of_labels[1].name },
            { name: "חלוואתאלגבן", description: "גליליות מילוי גבינה", price: 41, picture: 'HALWAT_ALJBEN.jpeg', type: special_set_of_labels[3].name },
            { name: "מעמול", description: "עוגת גבינה במילוי תמרים", price: 42, picture: 'MAMOL.jpeg', type: special_set_of_labels[1].name },
            { name: "קוקולידהמפרוקה", description: "שתי עוגיות במילוי של גלידה", price: 50, picture: 'KOKOLIDA_MAFROKA.jpeg', type: special_set_of_labels[3].name },

        ],
    }
}


//localStorage.setItem("arabic_resturant", JSON.stringify(arabic_resturant))

//localStorage.setItem("arabic_resturant_1", JSON.stringify(arabic_resturant))



arabic_resturant_help = {
    dishes: {
        main_cource: [
            { name: "פלאפל", description: "פיתה במילוי כדורי ופלאפל וירקות", price: 30, picture: 'falafal.jpeg', type: special_set_of_labels[1].name },
        ],
        dessert: [
            { name: "שבקיה", description: "בצק מטוגן מצופה דבש ושומשום", price: 40, picture: 'sbakia.jpeg', type: special_set_of_labels[3].name },
        ],
    }
}

localStorage.setItem("arabic_resturant_help", JSON.stringify(arabic_resturant_help));

