﻿<!DOCTYPE html>
<html>

<?php include("php/myphpadmin/insert.php"); ?>

<head>
    <meta charset="utf-8" />

    <title>FINAL HW</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script> -->
    <!-- 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>


    <style>
        body {
            background-image: url("./backgrounds/rest_back2.jpeg");
            background-repeat: repeat-y;
            /* background-color: #564845; */
            /* url("./backgrounds/rest_back2.jpeg") top no-repeat,  */
            /* background-image: url("./backgrounds/rest_back2_tiles.jpeg"); */
            /* background-size: 100%, 100%; */
        }


        label {
            display: block;
            font: .9rem 'Fira Sans', sans-serif;
        }

        .container {
            font-family: Calibri;
            text-align: center;
            height: 100%;

        }

        h2,
        h3,
        h4,
        h5,
        h1 {
            color: #bdc1c6;

        }

        #header {
            background-color: #00d6ff;
            font-size: 25px;
            font: bold;
        }

        .card-img-top {
            height: 150px;
        }

        .img {
            height: 50px;
            width: 50px;
        }

        #center-div {
            height: 250px;
            width: 250px;
        }

        #res {
            width: max-content;
        }
    </style>

    <!-- <script ></script> -->

    <script src="JavaScript.js"></script>
    <link href="StyleSheet.css" rel="stylesheet" />

</head>



<body>
    <form>
        <input type="button" value="click to refrsh menu" onclick="init_manager()">
        <br />
        <br />

    </form>


    <div class="container">
        <div class="topnav">
            <a class="active" href="index.php">בית-תפריט</a>
            <a href="hazmanot1.html">הזמנות</a>
            <a href="manager.php">תצוגת מנהל</a>
            <a href='status.html'>סטאטוס הזמנה</a>
            <a href='payment.html'>דף תשלום</a>
        </div>



        <br />



        <div class="row">

            <div id="left-div" class="col">

            </div>

            <div id="center-div" class="col-12">


                <div class="row">
                    <div id="new" class="col">
                        <h1 style="font-weight:900;text-decoration:underline;text-align:center;color:red">מנות
                            עיקריות</h1>
                    </div>

                </div>



                <div id="main_cource">

                    <!-- עיקריות -->

                </div>

                <div>

                    <h1 style="font-weight:900;text-decoration:underline;text-align:center;color:red">קינוחים</h1>

                </div>

                <div id="dessert">

                    <!-- קינוחים -->

                </div>

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" autoplay muted src="https://www.youtube.com/embed/xPPLbEFbCAo?autoplay=1&mute=1&controls=0&loop=1" type="video/mp4">
                    </iframe>
                </div>

                <div class="social_media_icons">
                    <div class="align-bottom">
                        <div class="center">
                            <ul class="social-network social-circle">
                                <li><a href="http://facebook.com" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="http://twitter.com" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="http://google.com" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="http://instagram.com" class="icoInstagram" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div id="right_div" class="col-xs-12 d-md-none d-lg-block col-xl-2"></div>
        </div>


        <button class="open-button" onclick="openForm()">הזמן שולחן</button>
        <div class="form-popup" id="myForm">
            <form method="post" class="form-container" id="popup-form">
                <h1>בצע הזמנה</h1>
                <p>אנא מלא את פרטי ההזמנה</p>
                <div class="col">

                    <input type="text" placeholder="שם" name="name" class="form-control" required>
                    <input type="number" placeholder="כמות אנשים" name="people_count" class="form-control" required>
                    <input name="date" id="input_date" required type="text" class="form-control" placeholder="תאריך" onfocus="(this.type='date')" />

                    <!-- <input type="date" value="תאריך" name="date" class="form-control" required> -->
                    <select class="form-control" id="inputGroupSelect01" name="area" required>
                        <option selected>בחר מיקום</option>
                        <option value="outside">בחוץ</option>
                        <option value="inside">בפנים</option>
                        <option value="bar">בר</option>
                        <option value=private_room">חדר פרטי (בתוספת תשלום)</option>
                    </select>
                </div>

                <br>

                <button type="submit" name="save" class="btn btn-lg btn-primary">הזמן</button>
                <button type="button" class="btn btn-lg btn-secondary" onclick="closeForm(this)">בטל</button>
            </form>
        </div>

        <script>
            function toast(text) {
                $('#toast-text').text(text)
                $('.toast').toast("show")
            }

            function testResponse() {

                var result = "<?php echo $result; ?>";
                if (result) {
                    toast(result)
                }
            }

            $(document).ready(testResponse)
        </script>




    </div>


    <div class="toast" data-autohide="true" data-delay="4000" style="position: absolute; top: 15px; right: 15px; width:400px; backdrop-filter:blur(10px)">
        <div class="toast-header">
            <svg class=" rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
                <rect fill="#007aff" width="100%" height="100%" />
            </svg>
            <strong class="mr-auto">PHP database</strong>
            <small class="text-muted"><?php echo date('H:i'); ?></small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body" id="toast-text">
            Default toast text
        </div>
    </div>


    <script>
        function openForm() {
            document.getElementById("myForm").style.display = "block";
        }

        function closeForm(object) {
            object.form.reset()
            document.getElementById("input_date").type = "text"
            document.getElementById("myForm").style.display = "none";
        }
    </script>


    <script>
        function init_manager() {

            localStorage.clear();
            localStorage.setItem("arabic_resturant", JSON.stringify(arabic_resturant))

            localStorage.setItem("arabic_resturant_1", JSON.stringify(arabic_resturant))

            var str = ""
            for (var i = 0; i < 10; i++) {

                str += `<div class="d-flex">`
                for (var ii = 0; ii < 2; ii++) {

                    if (ii == 1)
                        i = i + 1

                    str += `
                        <div id=item${i} class="col" style="
    overflow-wrap: anywhere;
" >
                            <img src="New folder/${arabic_resturant.dishes.main_cource[i].picture}"class="col img-responsive" style="width: 200px;
    height: 150px;" />

<h1>${arabic_resturant.dishes.main_cource[i].name}</h1>

    <h5> ${arabic_resturant.dishes.main_cource[i].price}$</h5>

        <h4>${arabic_resturant.dishes.main_cource[i].description}   </h4>

                <h5>${arabic_resturant.dishes.main_cource[i].type}</h5>
            
                 
                 
         </div>
`
                }
                str += "</div>"



            }
            document.getElementById("main_cource").innerHTML = str;


            for (var i = 0; i < 10; i++) {

                str += `<div class="d-flex">`
                for (var ii = 0; ii < 2; ii++) {

                    if (ii == 1)
                        i = i + 1

                    str += `
                        <div id=item${i} class="col" style="
    overflow-wrap: anywhere;
" >
                            <img src="New folder/${arabic_resturant.dishes.dessert[i].picture}"class="col img-responsive" style="width: 200px;
    height: 150px;" />

<h1>${arabic_resturant.dishes.dessert[i].name}</h1>

    <h5> ${arabic_resturant.dishes.dessert[i].price}$</h5>

        <h4>${arabic_resturant.dishes.dessert[i].description}   </h4>

                <h5>${arabic_resturant.dishes.dessert[i].type}</h5>
           
                 
                  
         </div>
`
                }
                str += "</div>"



            }
            document.getElementById("dessert").innerHTML = str;

        }
    </script>


    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>

</body>

</html>